package com.proyecto.appcampeonato.DataBase;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DbAdapter extends SQLiteOpenHelper {


    public DbAdapter(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, nombre, factory, version);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

        //aquí creamos la tabla de usuario (dni, nombre, ciudad, numero)
        db.execSQL("create table usuario(id integer primary key autoincrement, nombre String, ci text, pass text)");
        db.execSQL("create table campeonato(id integer primary key autoincrement, nombre String, iduser integer, estado integer)");
        db.execSQL("create table equipo(id integer primary key autoincrement, nombre String, idcampeonato integer, estado integer)");

        db.execSQL("create table sorteo(id integer primary key autoincrement, nombre String, equipoA integer, equipoB integer, valorA integer, valorB integer, idcampeonato, estado integer)");
        db.execSQL("create table resultado(id integer primary key autoincrement, valorA integer, valorB integer, idSorteo integer, estado integer)");

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

        db.execSQL("drop table if exists usuario");
        db.execSQL("drop table if exists campeonato");
        db.execSQL("drop table if exists equipo");
        db.execSQL("drop table if exists sorteo");
        db.execSQL("drop table if exists resultado");

        db.execSQL("create table usuario(id integer primary key autoincrement, nombre String, ci text, pass String)");
        db.execSQL("create table campeonato(id integer primary key autoincrement, nombre String, iduser integer, estado integer)");
        db.execSQL("create table equipo(id integer primary key autoincrement, nombre String, idcampeonato integer, estado integer)");
        db.execSQL("create table sorteo(id integer primary key autoincrement, nombre String, equipoA integer, equipoB integer, valorA integer, valorB integer, idcampeonato, estado integer)");
        db.execSQL("create table resultado(id integer primary key autoincrement, valorA integer, valorB integer, idSorteo integer, estado integer)");


    }

}