package com.proyecto.appcampeonato.IU_Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proyecto.appcampeonato.Add.AddEquipoActivity;
import com.proyecto.appcampeonato.Entidades.Equipo;
import com.proyecto.appcampeonato.R;

import java.util.ArrayList;

import static android.widget.Toast.makeText;

public class AdaptadorFollowersEquipo extends RecyclerView.Adapter<AdaptadorFollowersEquipo.ViewHolderFollowers> {

    TextView etiNombre, btndelete;
    String diequipo;



    public Context contextos;
    public Context mContext;


    ArrayList<Equipo> listaFollowers;

    public AdaptadorFollowersEquipo(ArrayList<Equipo> listaFollowers, Context contextos, Context mContext, AddEquipoActivity addEquipoActivity) {
        this.listaFollowers = listaFollowers;
        this.contextos = contextos;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_listequipo, parent, false);
        return new ViewHolderFollowers(view);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, final int position) {

        etiNombre.setText("Equipo: "+listaFollowers.get(position).getNombre());


//        lnCampeonato.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////
////                Intent home = new Intent(mContext, CampeonatoActivity.class);
////                home.putExtra("nameCamp",listaFollowers.get(position).getNombre());
////                home.putExtra("idCamp",listaFollowers.get(position).getId().toString());
////                mContext.startActivity(home);
////
////            }
////        });

    }


    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {
        public ViewHolderFollowers(View itemView) {
            super(itemView);
            etiNombre = (TextView) itemView.findViewById(R.id.txtname);

        }
    }




}
