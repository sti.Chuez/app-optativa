package com.proyecto.appcampeonato.IU_Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proyecto.appcampeonato.Add.AddEquipoActivity;
import com.proyecto.appcampeonato.Entidades.Campeonato;
import com.proyecto.appcampeonato.IU.CampeonatoActivity;
import com.proyecto.appcampeonato.IU.InicioActivity;
import com.proyecto.appcampeonato.R;

import java.util.ArrayList;

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    TextView etiNombre;
    TextView etiID;

    LinearLayout lnCampeonato;
    public Context contextos;
    public Context mContext;


    ArrayList<Campeonato> listaFollowers;

    public AdaptadorFollowers(ArrayList<Campeonato> listaFollowers, Context contextos, Context mContext, InicioActivity InicioActivity) {
        this.listaFollowers = listaFollowers;
        this.contextos = contextos;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_list, parent, false);
        return new ViewHolderFollowers(view);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, final int position) {

        etiID.setText("Capeonato: ");
        etiNombre.setText(listaFollowers.get(position).getNombre());


        lnCampeonato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent home = new Intent(mContext, AddEquipoActivity.class);
                home.putExtra("nameCamp",listaFollowers.get(position).getNombre());
                home.putExtra("idCamp",listaFollowers.get(position).getId().toString());
                mContext.startActivity(home);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {
        public ViewHolderFollowers(View itemView) {
            super(itemView);
            etiID = (TextView) itemView.findViewById(R.id.txtid);
            etiNombre = (TextView) itemView.findViewById(R.id.txtname);
            lnCampeonato = (LinearLayout) itemView.findViewById(R.id.LnCampeonato);
        }
    }


}
