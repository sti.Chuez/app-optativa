package com.proyecto.appcampeonato.IU_Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.proyecto.appcampeonato.Add.AddEquipoActivity;
import com.proyecto.appcampeonato.Entidades.Equipo;
import com.proyecto.appcampeonato.Entidades.Sorteo;
import com.proyecto.appcampeonato.R;

import java.util.ArrayList;

public class AdaptadorFollowersSorteo extends RecyclerView.Adapter<AdaptadorFollowersSorteo.ViewHolderFollowers> {

    TextView txteq1, txteq2;
    EditText valEq1, valEq2;

//    Button btnAct;




    public Context contextos;
    public Context mContext;


    ArrayList<Sorteo> listaFollowers;

    public AdaptadorFollowersSorteo(ArrayList<Sorteo> listaFollowers, Context contextos, Context mContext, AddEquipoActivity addEquipoActivity) {
        this.listaFollowers = listaFollowers;
        this.contextos = contextos;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_sorteo, parent, false);
        return new ViewHolderFollowers(view);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, final int position) {

        txteq1.setText(listaFollowers.get(position).getEquipoA());
        txteq2.setText(listaFollowers.get(position).getEquipoB());
        valEq1.setText(listaFollowers.get(position).getValorA().toString());
        valEq2.setText(listaFollowers.get(position).getValorB().toString());
    }


    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {
        public ViewHolderFollowers(View itemView) {
            super(itemView);

            txteq1 = (TextView) itemView.findViewById(R.id.textEq1);
            txteq2 = (TextView) itemView.findViewById(R.id.textEq2);
            valEq1 = (EditText) itemView.findViewById(R.id.valEq1);
            valEq2 = (EditText) itemView.findViewById(R.id.valEq2);

        }
    }




}
