package com.proyecto.appcampeonato;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;

import android.database.Cursor;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proyecto.appcampeonato.Add.RegisterActivity;
import com.proyecto.appcampeonato.DataBase.DbAdapter;
import com.proyecto.appcampeonato.IU.InicioActivity;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    private EditText TextCI, TextPass;
    private Button btnRegister, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        TextCI = (EditText) findViewById(R.id.txtCI);
        TextPass = (EditText) findViewById(R.id.txtPass);
        btnRegister = (Button) findViewById(R.id.btnregister);
        btnLogin = (Button) findViewById(R.id.btnlogin);


        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent register = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(register);
            }
        });


        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               validad();
            }
        });

    }


    public void validad(){
        if(TextCI.getText().toString()!="" && TextPass.getText().toString()!=""){
            LoginUser(TextCI.getText().toString(), TextPass.getText().toString());
        }else  Toast.makeText(getApplicationContext(), "Datos incompetos", Toast.LENGTH_LONG).show();
    }

    public void LoginUser(String ci, String pass) {

        DbAdapter admin = new DbAdapter(this, "usuario", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        Cursor fila = bd.rawQuery("select id, nombre from usuario where ci=" + ci +" and pass="+pass , null);

        if (fila.moveToFirst()) {
            Integer id = fila.getInt(0);
            String name = fila.getString(1);

            SharedPreferences preferences = getSharedPreferences("database", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("id",id.toString());
            editor.putString("name",name);
            editor.commit();
            finish();
            Intent inicio = new Intent(getApplicationContext(), InicioActivity.class);
            startActivity(inicio);

        } else{
            Toast.makeText(getApplicationContext(), "Usuario o Password incorrectos", Toast.LENGTH_LONG).show();
            TextPass.setText("");
        }
        bd.close();
    }




}

