package com.proyecto.appcampeonato.Add;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proyecto.appcampeonato.DataBase.DbAdapter;
import com.proyecto.appcampeonato.Entidades.Equipo;
import com.proyecto.appcampeonato.Entidades.Sorteo;
import com.proyecto.appcampeonato.IU.InicioActivity;
import com.proyecto.appcampeonato.IU_Adapter.AdaptadorFollowersEquipo;
import com.proyecto.appcampeonato.IU_Adapter.AdaptadorFollowersSorteo;
import com.proyecto.appcampeonato.R;

import java.util.ArrayList;

public class AddEquipoActivity extends AppCompatActivity {

    private ArrayList<Equipo> listaEquipo = new ArrayList<>();
    RecyclerView recyclerViewFollowersEquipo;

    private ArrayList<Sorteo> listaSorteo = new ArrayList<>();
    RecyclerView recyclerViewFollowersSorteo;

    private EditText textname;
    String nameCamp,idcamp;
    Button btnAdd,btnsorteo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_equipo);


        recyclerViewFollowersEquipo = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowersEquipo.setLayoutManager(new LinearLayoutManager(this));

        recyclerViewFollowersSorteo = (RecyclerView)findViewById(R.id.recyclerViewFollowersSorteo);
        recyclerViewFollowersSorteo.setLayoutManager(new LinearLayoutManager(this));

        textname = (EditText) findViewById(R.id.txtNombre);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnsorteo = (Button) findViewById(R.id.btnsorteo);

        Intent intent = getIntent();
        idcamp = intent.getExtras().getString("idCamp");
        nameCamp = intent.getExtras().getString("nameCamp");
        cargarCampeonato(idcamp);




        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               validar();
            }
        });


        btnsorteo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sorteo(); //realizar el sorteo
            }
        });




    }


    public void cargarCampeonato(String idcamp){
        //Creamos el cursor
        DbAdapter admin = new DbAdapter(this, "campeonato", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select id, nombre, estado from equipo where idcampeonato=" + idcamp, null);

        if (fila != null && fila.getCount()>0) {

            fila.moveToFirst();

            do {

                Integer id = fila.getInt(fila.getColumnIndex("id"));
                String nombre = fila.getString(fila.getColumnIndex("nombre"));
                Integer estado = fila.getInt(fila.getColumnIndex("estado"));

                Equipo equi = new Equipo(id,nombre,estado);
                listaEquipo.add(equi);

            } while (fila.moveToNext());

            AdaptadorFollowersEquipo dataAdapter = new AdaptadorFollowersEquipo(listaEquipo, this, this, AddEquipoActivity.this);
            recyclerViewFollowersEquipo.setAdapter(dataAdapter);

            fila.close();
        }else{
            //Toast.makeText(getApplicationContext(), "No se encontraron resultados", Toast.LENGTH_LONG).show();
            fila.close();
        }

    }

    public String registroEqui(String nombre, String idcamp, Integer estado) {

        DbAdapter campeonato = new DbAdapter(this, "campeonato", null, 1);
        SQLiteDatabase bd = campeonato.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("idcampeonato", idcamp);
        registro.put("estado", estado);
        // los inserto en la base de datos
        bd.insert("equipo", null, registro);
        bd.close();
        return "Equipo registrado Correctamente";
    }



    public void validar(){
        if( textname.getText().toString()!=""){
            String insert = registroEqui(textname.getText().toString(),idcamp, 0);
            textname.setText("");
            listaEquipo.clear();
            cargarCampeonato(idcamp);
        }else  Toast.makeText(getApplicationContext(), "Datos incompetos", Toast.LENGTH_LONG).show();
    }


//Generar sorte
    public void sorteo(){

//        Crear lista equipos
        ArrayList<Equipo> lista2 = new ArrayList<>();

        //clonar lista
        lista2.clear();
        lista2 = listaEquipo;

//obtener randon mientras la lista tenga datos
        while (lista2.size()!=0){

            String equipoA="Sin competidor", equipoB="Sin competidor";

            int lista;
            lista = lista2.size();
//obtener equipo A
            if (lista!=0){
                Double tar = Math.floor((Math.random() * lista-1)+1);
                int target = (tar).byteValue();
                equipoA=  lista2.get(target).getNombre();
                lista2.remove(target);
            }

            lista = lista2.size();
//Obtener equipo B
           if (lista!=0){
               Double tar2 = Math.floor((Math.random() * lista2.size()-1)+1);
               int target2 = (tar2).byteValue();
               equipoB = lista2.get(target2).getNombre();
               lista2.remove(target2);
           }


            String b= "Grupo";
//Asignar valores al constructor
            Sorteo sort = new Sorteo(b,equipoA,equipoB,0,0,Integer.parseInt(idcamp),0);
//Anadir a la lista
            listaSorteo.add(sort);
        }

//Asignar lista al adaptador
        AdaptadorFollowersSorteo dataAdapter = new AdaptadorFollowersSorteo(listaSorteo, this, this, AddEquipoActivity.this);
        recyclerViewFollowersSorteo.setAdapter(dataAdapter);

    }
}
