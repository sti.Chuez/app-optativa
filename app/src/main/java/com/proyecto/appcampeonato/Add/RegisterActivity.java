package com.proyecto.appcampeonato.Add;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proyecto.appcampeonato.DataBase.DbAdapter;
import com.proyecto.appcampeonato.R;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity {



    private EditText TextCI, TextName, TextPass;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);



        TextCI = (EditText) findViewById(R.id.txtCI);
        TextName = (EditText) findViewById(R.id.txtName);
        TextPass = (EditText) findViewById(R.id.txtPass);
        btnRegister = (Button) findViewById(R.id.btnregister);


        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
              validar();
            }
        });



    }


    public void validar(){
        if(TextCI.getText().toString()!="" && TextPass.getText().toString()!="" && TextName.getText().toString()!=""){

            String insert = registroUser(TextName.getText().toString(),TextCI.getText().toString(), TextPass.getText().toString());
            Toast.makeText(getApplicationContext(), insert, Toast.LENGTH_LONG).show();
            finish();

        }else  Toast.makeText(getApplicationContext(), "Datos incompetos", Toast.LENGTH_LONG).show();
    }



    // Registro de usuario en DataBase
    public String registroUser(String nombre, String ci, String pass) {

        DbAdapter usuario = new DbAdapter(this, "usuario", null, 1);
        SQLiteDatabase bd = usuario.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("ci", ci);
        registro.put("pass", pass);

        // los inserto en la base de datos
        bd.insert("usuario", null, registro);
        bd.close();

        return "Usuario registrado Correctamente";

    }


}

