package com.proyecto.appcampeonato.Add;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.proyecto.appcampeonato.DataBase.DbAdapter;
import com.proyecto.appcampeonato.IU.InicioActivity;
import com.proyecto.appcampeonato.R;

public class AddCampActivity extends AppCompatActivity {

    TextView txtname;
    Button btnadd;
    String iduser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_camp);

        txtname=(TextView) findViewById(R.id.txtName);
        btnadd=(Button) findViewById(R.id.btnadd);

        SharedPreferences preferences = getSharedPreferences("database", Context.MODE_PRIVATE);
        iduser = preferences.getString("id",null);


        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            validar();
            }
        });

    }



    public void validar(){
        if( txtname.getText().toString()!=""){
            String insert = regisCapm(txtname.getText().toString(), iduser, 0);
            Intent register = new Intent(getApplicationContext(), InicioActivity.class);
            startActivity(register);
        }else  Toast.makeText(getApplicationContext(), "Datos incompetos", Toast.LENGTH_LONG).show();
    }


    // Registro de usuario en DataBase
    public String regisCapm(String nombre, String iduser, Integer estado) {

        DbAdapter campeonato = new DbAdapter(this, "campeonato", null, 1);
        SQLiteDatabase bd = campeonato.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("iduser", iduser);
        registro.put("estado", estado);

        // los inserto en la base de datos
        bd.insert("campeonato", null, registro);
        bd.close();
        return "Campeonato registrado Correctamente";

    }


}
