package com.proyecto.appcampeonato.IU;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.proyecto.appcampeonato.R;

public class CampeonatoActivity extends AppCompatActivity {

    String idcamp, nameCamp;
    TextView textname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campeonato);

        Intent intent = getIntent();
        idcamp = intent.getExtras().getString("idCamp");
        nameCamp = intent.getExtras().getString("nameCamp");




        textname=(TextView) findViewById(R.id.txtnameCam);
        textname.setText(idcamp.toString());


    }
}
