package com.proyecto.appcampeonato.IU;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.proyecto.appcampeonato.Entidades.Campeonato;
import com.proyecto.appcampeonato.DataBase.DbAdapter;
import com.proyecto.appcampeonato.IU_Adapter.AdaptadorFollowers;
import com.proyecto.appcampeonato.R;

import java.util.ArrayList;

public class InicioActivity extends AppCompatActivity {


    private ArrayList<Campeonato> listaCampeonato = new ArrayList<>();
    RecyclerView recyclerViewFollowers;



    TextView textname, Editname;
    String userID;
    Button btnadd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        textname=(TextView) findViewById(R.id.txtname);
        Editname=(TextView) findViewById(R.id.Editname);

        btnadd=(Button) findViewById(R.id.btnadd);

        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences preferences = getSharedPreferences("database", Context.MODE_PRIVATE);
        textname.setText("Hola! " + preferences.getString("name",null));
        userID = preferences.getString("id",null);


        cargarCampeonato(userID);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String insert = Addcamp(Editname.getText().toString(),userID, 0);
               // Toast.makeText(getApplicationContext(), insert, Toast.LENGTH_LONG).show();
                textname.setText("");
                listaCampeonato.clear();
                cargarCampeonato(userID);
            }
        });
    }



        public void cargarCampeonato(String iduser){
            //Creamos el cursor
            DbAdapter admin = new DbAdapter(this, "campeonato", null, 1);
            SQLiteDatabase bd = admin.getWritableDatabase();

            Cursor fila = bd.rawQuery("select id, nombre from campeonato where iduser=" + iduser, null);

            if (fila != null && fila.getCount()>0) {

                fila.moveToFirst();

                do {
                    Integer id = fila.getInt(fila.getColumnIndex("id"));
                    String nombre = fila.getString(fila.getColumnIndex("nombre"));
                    Campeonato camp = new Campeonato(id,nombre);
                    listaCampeonato.add(camp);
                } while (fila.moveToNext());

                AdaptadorFollowers dataAdapter = new AdaptadorFollowers(listaCampeonato, this, this,InicioActivity.this);
                recyclerViewFollowers.setAdapter(dataAdapter);
                fila.close();
            }else{
                Toast.makeText(getApplicationContext(), "No se encontraron resultados", Toast.LENGTH_LONG).show();
                fila.close();
            }

        }

    public String Addcamp(String nombre, String iduser, Integer estado) {

        DbAdapter campeonato = new DbAdapter(this, "campeonato", null, 1);
        SQLiteDatabase bd = campeonato.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("iduser", iduser);
        registro.put("estado", estado);

        // los inserto en la base de datos
        bd.insert("campeonato", null, registro);
        bd.close();
        return "Campeonato registrado Correctamente";

    }

    public void deleteEquipo(String idEquipo){
        DbAdapter admin = new DbAdapter(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Integer cant = bd.delete("equipo", "id=" + idEquipo, null);

        bd.close();

        if (cant == 1)
            Toast.makeText(this, "Equipo eliminado", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Equipo no eliminado", Toast.LENGTH_SHORT).show();
    }


}



