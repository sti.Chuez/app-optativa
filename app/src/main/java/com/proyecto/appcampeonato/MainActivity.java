package com.proyecto.appcampeonato;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.Snackbar;



public class MainActivity extends AppCompatActivity {

    EditText equipo1, equipo2, equipo3, equipo4, equipo5, equipo6, equipo7, equipo8;
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        equipo1 = (EditText) findViewById(R.id.equipo1);
        equipo2 = (EditText) findViewById(R.id.equipo2);
        equipo3 = (EditText) findViewById(R.id.equipo3);
        equipo4 = (EditText) findViewById(R.id.equipo4);
        equipo5 = (EditText) findViewById(R.id.equipo5);
        equipo6 = (EditText) findViewById(R.id.equipo6);
        equipo7 = (EditText) findViewById(R.id.equipo7);
        equipo8 = (EditText) findViewById(R.id.equipo8);
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayout);

    }
    
    public void guardar(View view){
        if(equipo1.getText().toString().trim().isEmpty() || equipo2.getText().toString().trim().isEmpty()
                || equipo3.getText().toString().trim().isEmpty() || equipo4.getText().toString().trim().isEmpty()
                || equipo5.getText().toString().trim().isEmpty() || equipo6.getText().toString().trim().isEmpty()
                || equipo7.getText().toString().trim().isEmpty() || equipo8.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar.make(constraintLayout, "Ingrese 8 equipos", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.rgb(255, 255, 255));
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.rgb(198, 40, 40));
            TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.rgb(255, 255, 255));
            snackbar.show();
        }else {
            SharedPreferences.Editor editor = getSharedPreferences("Equipos", MODE_PRIVATE).edit();
            editor.putString("equipo1", equipo1.getText().toString());
            editor.putString("equipo2", equipo2.getText().toString());
            editor.putString("equipo3", equipo3.getText().toString());
            editor.putString("equipo4", equipo4.getText().toString());
            editor.putString("equipo5", equipo5.getText().toString());
            editor.putString("equipo6", equipo6.getText().toString());
            editor.putString("equipo7", equipo7.getText().toString());
            editor.putString("equipo8", equipo8.getText().toString());
            editor.commit();
            Intent Equipos = new Intent(getApplicationContext(), EquiposActivity.class);
            startActivity(Equipos);
            //finish();
        }
    }
}
