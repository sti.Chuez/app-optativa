package com.proyecto.appcampeonato;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class GraficoActivity extends AppCompatActivity {


    private ProgressDialog progressDialog;
    TextView equipo1, equipo2, equipo3, equipo4, equipo5, equipo6, equipo7, equipo8;
    String nombreEquipo1, nombreEquipo2, nombreEquipo3, nombreEquipo4, nombreEquipo5, nombreEquipo6,
            nombreEquipo7, nombreEquipo8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);
        equipo1 = (TextView) findViewById(R.id.equipo1);
        equipo2 = (TextView) findViewById(R.id.equipo2);
        equipo3 = (TextView) findViewById(R.id.equipo3);
        equipo4 = (TextView) findViewById(R.id.equipo4);
        equipo5 = (TextView) findViewById(R.id.equipo5);
        equipo6 = (TextView) findViewById(R.id.equipo6);
        equipo7 = (TextView) findViewById(R.id.equipo7);
        equipo8 = (TextView) findViewById(R.id.equipo8);
        mostrarEquipos();
    }

    public void mostrarEquipos(){
        SharedPreferences equipos = getSharedPreferences("EquipoosSorteados", MODE_PRIVATE);
        nombreEquipo1 = equipos.getString("equipo1", null);
        nombreEquipo2 = equipos.getString("equipo2", null);
        nombreEquipo3 = equipos.getString("equipo3", null);
        nombreEquipo4 = equipos.getString("equipo4", null);
        nombreEquipo5 = equipos.getString("equipo5", null);
        nombreEquipo6 = equipos.getString("equipo6", null);
        nombreEquipo7 = equipos.getString("equipo7", null);
        nombreEquipo8 = equipos.getString("equipo8", null);
        equipo1.setText(nombreEquipo1);
        equipo2.setText(nombreEquipo2);
        equipo3.setText(nombreEquipo3);
        equipo4.setText(nombreEquipo4);
        equipo5.setText(nombreEquipo5);
        equipo6.setText(nombreEquipo6);
        equipo7.setText(nombreEquipo7);
        equipo8.setText(nombreEquipo8);
    }
}
