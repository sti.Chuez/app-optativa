package com.proyecto.appcampeonato;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class EquiposActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    TextView equipo1, equipo2, equipo3, equipo4, equipo5, equipo6, equipo7, equipo8;
    String nombreEquipo1, nombreEquipo2, nombreEquipo3, nombreEquipo4, nombreEquipo5, nombreEquipo6,
            nombreEquipo7, nombreEquipo8;
    Button sorteo, ver_resultado;
    String[] equipos_sorteados;

    LinearLayout layout_cuarto, layout_semi, layout_final;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipos);
        progressDialog = new ProgressDialog(this);
        sorteo = (Button) findViewById(R.id.sorteo);
        ver_resultado = (Button) findViewById(R.id.resultado);
        ver_resultado.setVisibility(View.GONE);

        equipo1 = (TextView) findViewById(R.id.equipo1);
        equipo2 = (TextView) findViewById(R.id.equipo2);
        equipo3 = (TextView) findViewById(R.id.equipo3);
        equipo4 = (TextView) findViewById(R.id.equipo4);
        equipo5 = (TextView) findViewById(R.id.equipo5);
        equipo6 = (TextView) findViewById(R.id.equipo6);
        equipo7 = (TextView) findViewById(R.id.equipo7);
        equipo8 = (TextView) findViewById(R.id.equipo8);
        mostrarEquipos();

        layout_cuarto = (LinearLayout) findViewById(R.id.layout_cuartos);
        layout_semi = (LinearLayout) findViewById(R.id.layout_semi);
        layout_final = (LinearLayout) findViewById(R.id.layout_final);
        layout_semi.setVisibility(View.GONE);
        layout_final.setVisibility(View.GONE);

    }

    public void mostrarEquipos(){

        SharedPreferences equipos = getSharedPreferences("Equipos", MODE_PRIVATE);
        nombreEquipo1 = equipos.getString("equipo1", null);
        nombreEquipo2 = equipos.getString("equipo2", null);
        nombreEquipo3 = equipos.getString("equipo3", null);
        nombreEquipo4 = equipos.getString("equipo4", null);
        nombreEquipo5 = equipos.getString("equipo5", null);
        nombreEquipo6 = equipos.getString("equipo6", null);
        nombreEquipo7 = equipos.getString("equipo7", null);
        nombreEquipo8 = equipos.getString("equipo8", null);

        equipo1.setText(nombreEquipo1);
        equipo2.setText(nombreEquipo2);
        equipo3.setText(nombreEquipo3);
        equipo4.setText(nombreEquipo4);
        equipo5.setText(nombreEquipo5);
        equipo6.setText(nombreEquipo6);
        equipo7.setText(nombreEquipo7);
        equipo8.setText(nombreEquipo8);

    }
    
    public void sorteo(View view){
        progressDialog.setTitle("Espere");
        progressDialog.setMessage("Realizando sorteo de partidos...");
        progressDialog.show();
        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                sorteo.setVisibility(View.GONE);
                ver_resultado.setVisibility(View.VISIBLE);
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 10000);


        String[] equipos = {nombreEquipo1, nombreEquipo2, nombreEquipo3, nombreEquipo4, nombreEquipo5,
                nombreEquipo6, nombreEquipo7, nombreEquipo8 };
        int[] estado_sorteado = {0,0,0,0,0,0,0,0 };
        equipos_sorteados = new String[8];
        int cont = 0;
        int azar;

        while(estado_sorteado[0] == 0 || estado_sorteado[1] == 0 || estado_sorteado[2] == 0 ||
                estado_sorteado[3] == 0 || estado_sorteado[4] == 0 || estado_sorteado[5] == 0 ||
                estado_sorteado[6] == 0 || estado_sorteado[7] == 0){
            azar = (int) Math.floor(Math.random()* 8);
            if (estado_sorteado[azar] == 0){
                estado_sorteado[azar] = 1;
                equipos_sorteados[cont] = equipos[azar];
                cont += 1;
            }
        }
        sorteo.setVisibility(View.GONE);
        ver_resultado.setVisibility(View.VISIBLE);
    }

    public void verResultados(View view){
        SharedPreferences.Editor editor = getSharedPreferences("EquipoosSorteados", MODE_PRIVATE).edit();
        editor.putString("equipo1", equipos_sorteados[0].toString());
        editor.putString("equipo2", equipos_sorteados[1].toString());
        editor.putString("equipo3", equipos_sorteados[2].toString());
        editor.putString("equipo4", equipos_sorteados[3].toString());
        editor.putString("equipo5", equipos_sorteados[4].toString());
        editor.putString("equipo6", equipos_sorteados[5].toString());
        editor.putString("equipo7", equipos_sorteados[6].toString());
        editor.putString("equipo8", equipos_sorteados[7].toString());
        editor.commit();
        Intent Grafico = new Intent(getApplicationContext(), GraficoActivity.class);
        startActivity(Grafico);
    }
}
