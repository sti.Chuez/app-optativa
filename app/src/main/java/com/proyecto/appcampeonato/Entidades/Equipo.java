package com.proyecto.appcampeonato.Entidades;

public class Equipo {

    Integer id;
    String nombre;
    Integer estado;

//Parametros de la contula a la database
    public Equipo(Integer id,String nombre, Integer estado) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;

    }


    public Equipo() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}
