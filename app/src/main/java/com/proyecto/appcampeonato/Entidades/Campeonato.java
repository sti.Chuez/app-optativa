package com.proyecto.appcampeonato.Entidades;

public class Campeonato {

    Integer id;
    String nombre;


    public Campeonato(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;

    }


    public Campeonato() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
