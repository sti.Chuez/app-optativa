package com.proyecto.appcampeonato.Entidades;

public class Sorteo {

    Integer id;
    String nombre;
    String equipoA;
    String equipoB;
    Integer valorA;
    Integer valorB;
    Integer idcampeonato;
    Integer estado;


    public Sorteo(String nombre, String equipoA, String equipoB, Integer valorA, Integer valorB, Integer idcampeonato, Integer estado) {
        this.nombre = nombre;
        this.equipoA = equipoA;
        this.equipoB = equipoB;
        this.valorA = valorA;
        this.valorB = valorB;
        this.idcampeonato = idcampeonato;
        this.estado = estado;
    }

    public Sorteo(Integer id, String nombre, String equipoA, String equipoB, Integer valorA, Integer valorB, Integer idcampeonato, Integer estado) {
        this.id = id;
        this.nombre = nombre;
        this.equipoA = equipoA;
        this.equipoB = equipoB;
        this.valorA = valorA;
        this.valorB = valorB;
        this.idcampeonato = idcampeonato;
        this.estado = estado;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEquipoA() {
        return equipoA;
    }

    public void setEquipoA(String equipoA) {
        this.equipoA = equipoA;
    }

    public String getEquipoB() {
        return equipoB;
    }

    public void setEquipoB(String equipoB) {
        this.equipoB = equipoB;
    }

    public Integer getValorA() {
        return valorA;
    }

    public void setValorA(Integer valorA) {
        this.valorA = valorA;
    }

    public Integer getValorB() {
        return valorB;
    }

    public void setValorB(Integer valorB) {
        this.valorB = valorB;
    }

    public Integer getIdcampeonato() {
        return idcampeonato;
    }

    public void setIdcampeonato(Integer idcampeonato) {
        this.idcampeonato = idcampeonato;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}
